<?php

namespace Drupal\no_admin_destination_redirect;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The no admin destination redirect class.
 */
class NoAdminDestinationRedirect {

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * Constructs service.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Defines the configuration object factory.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   */
  public function __construct(
    ConfigFactory $config_factory,
    AdminContext $admin_context,
    RequestStack $request_stack,
    AccountProxyInterface $account_proxy
  ) {
    $this->configFactory = $config_factory;
    $this->adminContext = $admin_context;
    $this->requestStack = $request_stack;
    $this->accountProxy = $account_proxy;
  }

  /**
   * Check to see if destination redirect should be stripped or not.
   *
   * @return bool
   *   Returns TRUE to strip, FALSE otherwise.
   */
  public function isNoDestinationRedirect(): bool {
    $current_uri = $this->requestStack->getCurrentRequest()->getRequestUri();
    $current_user = $this->accountProxy->getAccount();

    if (empty($current_uri) || empty($current_user)) {
      return FALSE;
    }

    $roles = $current_user->getRoles();
    if (empty($roles)) {
      return FALSE;
    }

    if (!substr($current_uri, 0, 6) === "/admin/") {
      return FALSE;
    }

    if (!$this->adminContext->isAdminRoute()) {
      return FALSE;
    }

    if (!in_array('administrator', $roles)) {
      return FALSE;
    }

    $excluded_paths = $this->configFactory->get('no_admin_destination_redirect.settings');
    $excluded_paths = $excluded_paths->get('excluded_paths_config');
    if (!empty($excluded_paths)) {
      $excluded_paths = explode(PHP_EOL, $excluded_paths);
      if (in_array($current_uri, $excluded_paths)) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
