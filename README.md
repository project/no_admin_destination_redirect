## No Admin Destination Redirect

Disables the annoying ?destination=/admin/foo/bar redirects for
admins when administering a Drupal site.

E.g. After editing & then saving a view, you will not be redirected
back to the main views listing page.

This module only works if the current user is an administrator, and
is only applied to admin pages.

### Requirements

No requirements at this time.

### Install/Usage

* Enable module like any other contrib module.
* Excluded paths can be managed here: 
**/admin/config/no-admin-destination-redirect**
* Flush cache.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
