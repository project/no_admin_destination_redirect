(function ($, Drupal) {
  Drupal.behaviors.no_admin_destination_redirect = {
    attach: function (context, settings) {
      $('body').find('.contextual-links a').each(function () {
        let uri = URI(this.getAttribute('href')).removeSearch('destination');
        this.setAttribute('href', uri.toString());
      });
    }
  };
}(jQuery, Drupal));
